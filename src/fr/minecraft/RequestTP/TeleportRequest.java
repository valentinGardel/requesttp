package fr.minecraft.RequestTP;
import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class TeleportRequest {
	private static ArrayList<TeleportRequest> TP_REQUESTS = new ArrayList<TeleportRequest>();
	public Player player, cible;
	public boolean tpa;
	public Location location;
	
	/**
	 * Constructeur d une requete de teleportation
	 * @param playerToTeleport est le joueur a qui on veut se teleporter
	 * @param playerToDecide est le joueur a qui on demande
	 * @param requestIsTpa est true si le joueur qui doit etre tp est le player
	 */
	public TeleportRequest(Player playerToTeleport, Player playerToDecide, boolean requestIsTpa){
		this.tpa = requestIsTpa;
		this.cible = playerToDecide;
		if(requestIsTpa)
			this.location = playerToDecide.getLocation();
		else
			this.location = playerToTeleport.getLocation();
		this.player = playerToTeleport;
	}
	
	public boolean accept()
	{
		if(this.tpa)
		{
			if(this.tpa)
			{
				if(this.player.isOnline())
				{
					this.player.teleport(this.location);
					this.cible.sendMessage(ChatColor.GREEN+"Requete accept�");
				}
				else
					this.cible.sendMessage(ChatColor.RED+"Le joueur est d�connect�, impossible de le t�l�porter.");
			}
			else
			{
				this.cible.teleport(this.location);
				if(this.player.isOnline())
					this.player.sendMessage(ChatColor.GREEN+"Requete accept�");
			}
			TeleportRequest.remove(this);
		}
		return true;
	}
	
	public static boolean remove(TeleportRequest request)
	{
		return TP_REQUESTS.remove(request);
	}
	
	public static boolean add(TeleportRequest request)
	{
		return TP_REQUESTS.add(request);
	}
	
	public static TeleportRequest getTeleportRequest(Player p)
	{
		for(TeleportRequest i : TP_REQUESTS) {
			if(i.cible == p)
				return i;
		}
		return null;
	}

	public static TeleportRequest getTeleportRequest(Player player, Player cible) {
		for(TeleportRequest i : TP_REQUESTS) {
			if(i.cible == cible && i.player==player)
				return i;
		}
		return null;
	}
}