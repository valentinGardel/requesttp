package fr.minecraft.RequestTP;

import Commons.MinecraftPlugin;
import fr.minecraft.RequestTP.Commands.*;

@SuppressWarnings("unchecked")
public class Main extends MinecraftPlugin
{
	static {
		PLUGIN_NAME = "RequestTP";
		LOG_TOGGLE = false;
		COMMANDS = new Class[]{
				TeleportExecutor.class
			};
		LISTENERS = null;
		CONFIG = null;
	}

	public static Main PLUGIN;
	
	public void onEnable()
	{
		super.onEnable();
		PLUGIN = this;
	}
	
	@Override
	protected void initDatabase() throws Exception {}
}
