package fr.minecraft.RequestTP.Commands;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitScheduler;

import Commands.MinecraftCommand;
import Commons.MessageBuilder;
import fr.minecraft.RequestTP.Main;
import fr.minecraft.RequestTP.TeleportRequest;

/**
 * Command executor of commands: tpa, tphere, tpaccept, tpdeny, tpcancel
 * */
public class TeleportExecutor extends MinecraftCommand
{
	static {
		COMMAND_NAME = "tpa";
		TAB_COMPLETER = null;
	}
	
	/** Time before the request is auto-canceled */
	private static final long EXPIRATION_TIME = 1200*2;

	/**
	 * Function called when the command is used
	 * */
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if(sender instanceof Player)
		{
			Player player = (Player)sender;
			if(args.length == 1)
			{
				Player cible = Bukkit.getServer().getPlayer(args[0]);
				if(cible != null && cible.isOnline() && cible != player)
				{
					if(this.isOnGround(cible.getLocation()) && this.isOnGround(player.getLocation()))
					{
						if(label.equalsIgnoreCase("tpa"))
							this.TpA(player, cible);
						else if(label.equalsIgnoreCase("tphere"))
							this.TpHere(player, cible);
						else if(label.equalsIgnoreCase("tpcancel"))
							this.TpCancel(player, cible);
						else
							player.sendMessage(ChatColor.RED+"Commande invalide.");
					}
					else sender.sendMessage(ChatColor.RED+"Vous ou "+args[0]+" ne touchez pas le sol, commande annul�!");
				}
				else
					player.sendMessage(ChatColor.RED+"Le joueur "+args[0]+" est introuvable!");
			}
			else if(args.length == 0)
			{
				if(label.equalsIgnoreCase("tpaccept"))
					this.TpAccept(player);
				else if(label.equalsIgnoreCase("tpdeny"))
					this.TpDeny(player);
				else
					player.sendMessage(ChatColor.RED+"Commande invalide.");
			}
			else
				player.sendMessage(ChatColor.RED+"Commande invalide.");
		}
		return true;
	}

	/**
	 * Verify if a location touch the ground, used on player location
	 * @param location location
	 * @return true if the location touch the ground, or else false
	 * */
	private boolean isOnGround(Location location)
	{
		return !location.getBlock().isPassable() || !location.subtract(0, 1, 0).getBlock().isPassable() || !location.subtract(0, 2, 0).getBlock().isPassable();
	}
	
	/**
	 * Command tpa executor
	 * */
	private void TpA(Player player, Player cible)
	{
		TeleportRequest request = new TeleportRequest(player, cible, true);
		TeleportRequest.add(request);
		
		MessageBuilder.create(ChatColor.GOLD+"Le joueur "+player.getName()+" souhaite se t�l�porter � vous:")
			.onClickRunCommand("/tpaccept").onHoverText("Accepter").write(ChatColor.DARK_GREEN+"[Accepter]")
			.onClickRunCommand("/tpdeny").onHoverText("Refuser").write(ChatColor.DARK_RED+"[Refuser]")
			.send(cible);
		
		MessageBuilder.create(ChatColor.GREEN+"Requete tpa envoy�!")
			.onClickRunCommand("/tpcancel "+cible.getName()).onHoverText("Annuler").write(ChatColor.DARK_RED+"[Annuler]")
			.send(player);
		
		this.timer(request);
	}

	/**
	 * Command tphere executor
	 * */
	private void TpHere(Player player, Player cible)
	{
		TeleportRequest request = new TeleportRequest(player, cible, false);
		TeleportRequest.add(request);
		
		MessageBuilder.create(ChatColor.GOLD+"Le joueur "+player.getName()+" souhaite vous t�l�porter � lui:")
			.onClickRunCommand("/tpaccept").onHoverText("Accepter").write(ChatColor.DARK_GREEN+" [Accepter]")
			.onClickRunCommand("/tpdeny").onHoverText("Refuser").write(ChatColor.DARK_RED+" [Refuser]")
			.send(cible);
		
		MessageBuilder.create(ChatColor.GREEN+"Requ�te tphere envoy�e!")
			.onClickRunCommand("/tpcancel "+cible.getName()).onHoverText("Annuler").write(ChatColor.DARK_RED+" [Annuler]")
			.send(player);
		
		this.timer(request);
	}

	/**
	 * Allow to auto-cancel a request 
	 * */
	private void timer(TeleportRequest request)
	{
		BukkitScheduler scheduler = Bukkit.getServer().getScheduler();
		scheduler.runTaskLater(Main.PLUGIN, new Runnable() {
			@Override
			public void run() {
				if(TeleportRequest.remove(request))
				{
					String requestType = (request.tpa)?"tpa":"tphere";
					request.player.sendMessage(ChatColor.GOLD+"La requ�te "+requestType+" au joueur "+request.cible.getName()+" a expir�e.");
				}
			}
		}, EXPIRATION_TIME);
	}

	/**
	 * Command tpaccept executor
	 * */
	private void TpAccept(Player player)
	{
		TeleportRequest request = TeleportRequest.getTeleportRequest(player);
		if(request != null)
		{
			if(this.isOnGround(request.location) && this.isOnGround(player.getLocation()))
			{
				request.accept();
			}
			else 
			{
				request.cible.sendMessage(ChatColor.RED+"La position de t�l�portation n'est pas s�curis�! (elle ne touche pas le sol)");
			}
		}
		else
			player.sendMessage(ChatColor.RED+"Requ�te de t�l�portation introuvable(pour rappel une requete expire au bout de "+EXPIRATION_TIME/1200+" minute(s) )");
	}

	/**
	 * Command tpdeny executor
	 * */
	private void TpDeny(Player player)
	{
		TeleportRequest request = TeleportRequest.getTeleportRequest(player);
		if(request != null) {
			if(request.player.isOnline())
				request.player.sendMessage(ChatColor.RED+"Requ�te refus�e.");
			TeleportRequest.remove(request);
			request.cible.sendMessage(ChatColor.GREEN+"Requ�te refus�e.");
		}
		else
			player.sendMessage(ChatColor.RED+"Requ�te de t�l�portation introuvable(pour rappel une requete expire au bout de "+EXPIRATION_TIME/1200+" minute(s) )");
	}
	
	/**
	 * Command tpcancel executor
	 * */
	private void TpCancel(Player player, Player cible)
	{
		TeleportRequest request = TeleportRequest.getTeleportRequest(player, cible);
		if(request != null) {
			if(request.player.isOnline())
				request.player.sendMessage(ChatColor.GREEN+"Requ�te annul�e.");
			TeleportRequest.remove(request);
			request.cible.sendMessage(ChatColor.RED+"Requ�te annul�e.");
		}
		else
			player.sendMessage(ChatColor.RED+"Requ�te de t�l�portation introuvable(pour rappel une requete expire au bout de "+EXPIRATION_TIME/1200+" minute(s) )");
	}

	
}
